package tercerDeber;

import java.util.Scanner;

/**
 * Created by Bryan on 30/05/2017.
 */
public class Modulo {

    public static Scanner scanner = new Scanner(System.in); //Leer el texto por consola

    byte b1,b2,b3;
    boolean validar;
    int ex,ba,ex1,ba1,r1;

    public void setModulo()
    {
                System.out.println("Ingrese el primer valor: ");
                b1 = scanner.nextByte();
                System.out.println("Ingrese el segundo valor: ");
                b2 = scanner.nextByte();

                if(b2==0)
                {
                    System.out.println("El segundo numero no puede ser cero ingrese otro: ");
                    b3 = scanner.nextByte();
                    System.out.println("El modulo es:" + (b1 % b3));
                    if((b1%b3)>=1)
                    {
                        validar=true;
                    }else if((b1%b3)<1)
                        {
                            validar=false;
                        }
                    if(validar==true)
                    {
                        //Boolean a String dos formas ///////////////////////////////////////////////////////////////////////////////////
                        //String cadena = String.valueOf(validar);
                        String cadena = Boolean.toString(validar);
                        System.out.println("Se sumo 1 al modulo el resultado es: " + (1+(b1%b3)) + "Comprobando la validacion: " + cadena);
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }else {System.out.println("Se resto 1 al modulo" + ((b1%b3)-1));}
                }
                else {
                    System.out.println("El modulo es:" + (b1 % b2));
                    if((b1%b2)>=1)
                    {
                        validar=true;
                    }else if((b1%b2)<1)
                    {
                        validar=false;
                    }
                    if(validar==true)
                    {
                        System.out.println("Se sumo 1 al modulo" + (1+(b1%b2)));
                    }else {System.out.println("Se resto 1 al modulo el resultado es: " + ((b1%b2)-1));}
                }

    }

    public void setExponenciacion()
    {
        System.out.println("Ingrese la base: ");
        ba = scanner.nextInt();
        System.out.println("Ingrese el exponente: ");
        ex = scanner.nextInt();

        if(ba>1000000 || ba<-1000000 || ex>1000000 || ex<-1000000)
        {
            System.out.println("Los datos ingresados son incorrectos ingrese otros");
            System.out.println("Ingrese la base: ");
            ba1 = scanner.nextInt();
            System.out.println("Ingrese el exponente: ");
            ex1 = scanner.nextInt();

            r1=((ba1)^ex1);

            //Integer a string
            //String cadena = Integer.toString(r1);
            String cadena = String.valueOf(r1);
            System.out.println("El resultado es: " +(r1) + cadena);
        }
        else
            {
                int r2;
                r2 =(ba)^ex;

                //Integer a String dos formas/////////////////
                //String cadena = Integer.toString(r2);
                String cadena = String.valueOf(r2);
                //////////////////////////////////////////////

                //String a char///////////////////////////////
                char caracter = cadena.charAt(0);
                //////////////////////////////////////////////

                //char a String///////////////////////////////
                String cadena1 = Character.toString(caracter);
                //////////////////////////////////////////////

                //String a Integer dos formss/////////////////
                // int entero = Integer.parseInt(cadena1);
                Integer entero = Integer.valueOf(cadena1);

                /////////////////////////////////////////////

                //String a Double////////////////////////////
                double doble = Double.parseDouble(cadena1);
                ////////////////////////////////////////////

                //String a float///////////////////////////
                float flotante = Float.parseFloat(cadena1);
                //////////////////////////////////////////

                //String a boolean dos formas///////////////
                //Boolean bool = Boolean.valueOf();
                Boolean bool = Boolean.parseBoolean(cadena1);
                if(r2 > 1)
                {
                    bool = true;
                }else
                {
                    bool=false;
                }
                /////////////////////////////////////////////
            System.out.println("El resultado es: " +r2 + "El valor [0] es: " + caracter + "El entero del indice 0 es: "+ entero);
            System.out.println("Conertido a double el indice 0 es: " + doble + "El flotante es: " + flotante);
            System.out.println("Si el exponente es mayor a uno es verdadero caso contrarrio es falso, la respuesta es: " + bool);
            //System.out.println("");

            }

    }

}
