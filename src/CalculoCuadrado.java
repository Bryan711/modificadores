import java.util.Scanner;
import SegundoDeber.NombresFigurasGeometricas;
/**
 * Created by Bryan on 07/05/2017.
 */
public class CalculoCuadrado {

    public static Scanner scanner = new Scanner(System.in); //Leer el texto por consola

    //Deafult
    static double lado1;
    static double lado2;

    public  void setCuadrado()
    {
        System.out.println("Ingrese el valor del primer lado: ");
        lado1 = scanner.nextDouble();
        System.out.println("Ingrese el valor del segundo lado: ");
        lado2 = scanner.nextDouble();

        if(lado1==lado2)
        {
        NombresFigurasGeometricas nfg = new NombresFigurasGeometricas("Cuadrado","Regular");
        System.out.println(nfg.getNombre() + " " + nfg.getTipo());
        }else
            {
                NombresFigurasGeometricas nfg = new NombresFigurasGeometricas("Cuadrado","Irregular");
                System.out.println(nfg.getNombre() + " " +nfg.getTipo());
            }
    }

    public  void getCuadrado()
    {
        if(lado1==lado2)
        {
        System.out.println("El area es: "+(lado1*lado1));
        System.out.println("El perimetro es: "+(lado1+lado1+lado1+lado1));
        }
        else
            {
                System.out.println("El area es: "+(lado1*lado2));
                System.out.println("El perimetro es: "+(lado1+lado2+lado1+lado2));
            }
    }
}
