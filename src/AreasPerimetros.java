/**
 * Created by Bryan on 07/05/2017.
 */

import java.awt.*;
import java.util.Scanner;
import SegundoDeber.NombresFigurasGeometricas;
import tercerDeber.Modulo;

import javax.swing.*;

public class AreasPerimetros {

    public static Scanner scanner = new Scanner(System.in); //Leer el texto por consola
    public static int seleccionar = -1; //opción a elegir del usuario
    public static char seleccionar1 ='N';

    public static void main(String[] args) {

        //Mientras la opción elegida sea 0, preguntamos al usuario
        while(seleccionar != 0){
            //Try catch para evitar que el programa termine si hay un error
            try{
                System.out.println("\tCalculo de areas y perimetros");
                System.out.println(             "\n1.- Circulo" +
                                                "\n2.- Cuadrado" +
                                                "\n3.- Poligono Regular" +
                                                "\n4.- Triangulo" +
                                                "\n5.- Calculadora"+
                                                "\n0.- Salir");
                //Recoger una variable por consola
                System.out.println("Elija una opcion: ");
                seleccionar = Integer.parseInt(scanner.nextLine());

                switch(seleccionar){
                    case 1:
                        Calculocirculo cc = new Calculocirculo();
                        cc.setCirculo();
                        cc.getCirculo();
                        cc.setAngulo();
                        break;
                    case 2:
                        CalculoCuadrado ca = new CalculoCuadrado();
                        ca.setCuadrado();
                        ca.getCuadrado();
                        break;
                    case 3:
                        CalculoPentagonoRegular cp = new CalculoPentagonoRegular();
                        cp.setTipo();
                        cp.setPentagono();
                        //cp.setPentagonoAP();
                        break;
                    case 4:
                        CalculoTriangulo ct = new CalculoTriangulo();
                        ct.setTriangulo();
                        ct.getTriangulo();
                        ct.setTriangulol();
                        ct.getTriangulol();
                        break;
                    case 5:
                        String cadena = Character.toString(seleccionar1);
                        char comparar = 'S';
                        String cadena1 = Character.toString(comparar);
                        while(cadena != cadena1) {
                        try{
                            System.out.println("\tCalculadora");
                            System.out.println( "\n1.- Suma" +
                                                "\n2.- Resta" +
                                                "\n3.- Multiplicacion" +
                                                "\n4.- Division" +
                                                "\n5.- Modulo" +
                                                "\n6.- Exponenciacion" +
                                                "\n0.- Salir");
                            //Recoger una variable por consola
                            System.out.println("Elija una opcion: ");
                            seleccionar = Integer.parseInt(scanner.nextLine());

                            switch(seleccionar){
                                case 1:
                                    Suma s = new Suma();
                                    s.setSuma();
                                    break;
                                case 2:
                                    Suma r = new Suma();
                                    r.setResta();
                                    break;
                                case 3:
                                    Suma m = new Suma();
                                    m.setMultplicacion();
                                    break;
                                case 4:
                                    Suma d = new Suma();
                                    d.setDivision();
                                    d.getDivision();
                                    break;
                                case 5:
                                    Modulo b = new Modulo();
                                    b.setModulo();
                                    break;
                                case 6:

                                    break;
                                case 0:
                                    char letra;
                                        System.out.println("Esta seguro que desea salir?? [Presione [S] para salir]");
                                        letra = scanner.next().charAt(0);
                                    if(letra =='S'){System.out.println("Adios");}
                                    break;
                                default:
                                    System.out.println("Número incorrecto");
                                    break;
                            }
                        }catch(Exception e){
                            System.out.println("Error!");
                        }
                        }
                        break;
                    case 0:
                        char letra;
                        System.out.println("Esta seguro que desea salir?? [Presione [S] para salir]");
                        letra = scanner.next().charAt(0);
                        if(letra =='S'){System.out.println("Adios");}
                        break;
                    default:
                        System.out.println("Número incorrecto");
                        break;
                }
            }catch(Exception e){
                System.out.println("Error!");
            }
        }

    }
}
