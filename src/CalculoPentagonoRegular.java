import java.util.Scanner;
import SegundoDeber.NombresFigurasGeometricas;
/**
 * Created by Bryan on 07/05/2017.
 */
public class CalculoPentagonoRegular {

    public static Scanner scanner = new Scanner(System.in); //Leer el texto por consola

    //Protected
    //protected static double lado;
    static private double apo;
    public int ladosFG;

    //herencia
    public class CalcularApotema extends NombresFigurasGeometricas
    {
        private double apotema;

        public CalcularApotema(int lados, int radio)
        {
            super(lados,radio);
        }

        public void setApotema() {
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            nfg.setRadio();
            nfg.setLados();
            apotema = Math.sqrt((nfg.getRadio()*nfg.getRadio())-((nfg.getLados()/2)*(nfg.getLados()/2)));
            System.out.println("El apotema es: " + apotema);

            System.out.println("El area es: "+((ladosFG*nfg.getLados()*apotema)/2));

            System.out.println("El perimetro es: "+(nfg.getLados()*5));
        }

        public double getApotema() {
            return apotema;
        }

        public  void CalcularApotema()
        {
            setApotema();
           // System.out.println("El valor del apotema es: " + getApotema());
        }
    }

    public void setTipo()
    {
        System.out.println("Ingrese el numero de lados de la figura geometrica, debe ser mayor a 4 y menor a 6: ");
        ladosFG = scanner.nextInt();
        if(ladosFG<=4)
        {
            System.out.println("Dato no permitido, ingrese otro: ");
            ladosFG = scanner.nextInt();
            if(ladosFG==5)
            {
                NombresFigurasGeometricas nfg = new NombresFigurasGeometricas("Pentagono");
                System.out.println(nfg.nombrePE);
            }else if (ladosFG==6)
            {
                NombresFigurasGeometricas nfg = new NombresFigurasGeometricas("Exagono");
                System.out.println(nfg.nombrePE);
            }
        }
        else if(ladosFG==5)
        {
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas("Pentagono");
            System.out.println(nfg.nombrePE);
        }else if (ladosFG==6)
            {
                NombresFigurasGeometricas nfg = new NombresFigurasGeometricas("Exagono");
                System.out.println(nfg.nombrePE);
            }
    }

    public  void setPentagono()
    {
        if(ladosFG==5)
        {
            //System.out.println("Introduce el valor del lado del pentagono:");
            CalcularApotema ca1 = new CalcularApotema(5,0);
            ca1.CalcularApotema();
        }
        else if(ladosFG==6)
        {
            //System.out.println("Introduce el valor del lado del hexagono:");
            //lado = scanner.nextDouble();
            CalcularApotema ca2 = new CalcularApotema(6,0);
            ca2.CalcularApotema();
            NombresFigurasGeometricas nfg2 = new NombresFigurasGeometricas();
            System.out.println("El area es: "+((ladosFG*nfg2.getLados()*ca2.getApotema())/2));
            System.out.println("El perimetro es: "+(nfg2.getLados()*6));

        }
    }

    /*public  void setPentagonoAP()
    {
        if(ladosFG==5)
        {
            CalcularApotema ca = new CalcularApotema(56,0);
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            System.out.println("El area es: "+((ladosFG*nfg.getLados()*ca.getApotema())/2));
            System.out.println("El perimetro es: "+(nfg.getLados()+nfg.getLados()+nfg.getLados()+nfg.getLados()+nfg.getLados()));
        }
        else if(ladosFG==6)
        {
            CalcularApotema ca = new CalcularApotema(56,0);
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            System.out.println("El area es: "+((ladosFG*nfg.getLados()*ca.getApotema())/2));
            System.out.println("El perimetro es: "+(nfg.getLados()+nfg.getLados()+nfg.getLados()+nfg.getLados()+nfg.getLados()));
        }
    }*/


}
