package SegundoDeber;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bryan on 16/05/2017.
 */
public class NombresFigurasGeometricas {

    public static Scanner scanner = new Scanner(System.in); //Leer el texto por consola

    //cuadrado
    public String nombre;
    public String Tipo;

    //triangulo
    protected String nombreT;
    protected String tipoE, tipoI;

    //PE
    private int lados;
    private double radio;
    public String nombrePE;

    public NombresFigurasGeometricas(int Lados, int Radio)
    {
        this.lados = Lados;
        this.radio = Radio;
    }

    public void setLados() {
        System.out.println("Ingrese el valor de un lado: ");
        lados = scanner.nextInt();
    }

    public void setRadio() {
        System.out.println("Ingrese el valor del radio: ");
        radio = scanner.nextDouble();
    }

    public int getLados() {
        return lados;
    }

    public double getRadio() {
        return radio;
    }

    //constructro con parametros
    public  NombresFigurasGeometricas(String Nombre, String TipoFigura)
    {
        nombre = Nombre;
        Tipo = TipoFigura;
    }

    public String getNombre() { return nombre;  }

    public String getTipo() { return Tipo; }

    //constructor sin parametro
    // y sobrecarga de constructores
    public  NombresFigurasGeometricas()
    {
        nombreT = "Triangulo";
        tipoE = "Equilatero";
        tipoI = "Isosceles";
    }

    public String getNombreT() { return nombreT; }

    public String getTipoE() { return tipoE;}

    public String getTipoI() { return tipoI;}

    public  NombresFigurasGeometricas(String NombrePE)
    {
        nombrePE = NombrePE;
    }
}
