import java.util.Scanner;
import SegundoDeber.NombresFigurasGeometricas;
/**
 * Created by Bryan on 07/05/2017.
 */
public class CalculoTriangulo {

    protected static Scanner scanner= new Scanner(System.in);
    //protected and public
    protected static double base;
    public static double altura;

    private static double Triangulolado1;
    private static double Triangulolado2;
    private static double Triangulolado3;

    protected void setTriangulo()
    {
        System.out.println("Ingrese el valor de la base del triangulo: ");
        base = scanner.nextDouble();
        System.out.println("Ingrese el valor de la altura del triangulo: ");
        altura = scanner.nextDouble();
        System.out.println("\n");
    }

    protected void setTriangulol()
    {
        System.out.println("Ingrese el valor del primer lado del triangulo: ");
        Triangulolado1 = scanner.nextDouble();
        System.out.println("Ingrese el valor del segundo lado del triangulo: ");
        Triangulolado2 = scanner.nextDouble();
        System.out.println("Ingrese el valor del tercer lado del triangulo: ");
        Triangulolado3 = scanner.nextDouble();

        if (Triangulolado1==Triangulolado2 && Triangulolado1==Triangulolado3)
        {
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            System.out.println(nfg.getNombreT() + " " + nfg.getTipoE());
        }
        else if(Triangulolado1==Triangulolado2 && Triangulolado1!=Triangulolado3)
        {
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            System.out.println(nfg.getNombreT() + " " + nfg.getTipoI());
        }
        else if(Triangulolado1==Triangulolado3 && Triangulolado1!=Triangulolado2)
        {
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            System.out.println(nfg.getNombreT() + " " + nfg.getTipoI());
        }
        else if(Triangulolado3==Triangulolado2 && Triangulolado1!=Triangulolado3)
        {
            NombresFigurasGeometricas nfg = new NombresFigurasGeometricas();
            System.out.println(nfg.getNombreT() + " " + nfg.getTipoI());
        }
    }

    protected void  getTriangulo()
    {
        System.out.println("El area es: " +((base*altura)/2));
    }

    protected  void getTriangulol()
    {
        System.out.println("El perimetro del triangulo es: "+(Triangulolado1+Triangulolado2+Triangulolado3));
    }
}


