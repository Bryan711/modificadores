import java.util.Scanner;

/**
 * Created by Bryan on 07/05/2017.
 */
public class Calculocirculo {

    public static Scanner scanner = new Scanner(System.in); //Leer el texto por consola

    //Private //final
    private static final double pi = 3.141592654;
    static private double r;
    static public int pa;

    //metodo enum
    public enum Pigrados {

        NOVENTA ("PRIMER"), //Separamos con comas
        CINETOOCHENTA ("SEGUNDO"),
        DOSCIENTOSSETENTA("TERCER"),
        TRESCIENTOSSESENTA ("CUARTO");

        //Campos tipo constante
        private String cuadrante;

        Pigrados (String Cuadrante) {
            cuadrante = Cuadrante;
        } //Cierre del constructor

        //Métodos de la clase tipo Enum
        public String getPi() { return cuadrante; }
    }

    public  void setAngulo()
    {
        System.out.println("Introduce el angulo del circulo en grados:");
        pa = scanner.nextInt();
        if(pa>=0&&pa<=90)
        {
            Pigrados pg = Pigrados.NOVENTA;
            System.out.println("El angulo se encuentra en el " + pg.getPi() + " cuadrante");
        }
        else if(pa>90 && pa<=180)
        {
            Pigrados pg = Pigrados.CINETOOCHENTA;
            System.out.println("El angulo se encuentra en el " + pg.getPi() + " cuadrante");
        }
        else if(pa>180 && pa<=270)
        {
            Pigrados pg = Pigrados.DOSCIENTOSSETENTA;
            System.out.println("El angulo se encuentra en el " + pg.getPi() + " cuadrante");
        }
        else if(pa>270 && pa<=360)
        {
            Pigrados pg = Pigrados.TRESCIENTOSSESENTA;
            System.out.println("El angulo se encuentra en el " + pg.getPi() + " cuadrante");
        }
            //System.out.println("\n");
    }//fin enum

    public  void setCirculo()
    {
        System.out.println("Introduce el radio del circulo:");
        r = scanner.nextDouble();
        //System.out.println("\n");
    }

    public  void getCirculo()
    {
        System.out.println("El area es: "+(pi*r*r));
        System.out.println("El perimetro es: "+(2*pi*r));
    }
}
